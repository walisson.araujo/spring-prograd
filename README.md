List of endpoints:

- Student; 
> POST to create a Student;
> "student/"

> GET findALL to search all Student in a List;
> "student/findAll"

> GET findOne to search one Student in a List;
> "student/findOne"
- Professor;

> POST to create a Professor;
"professor/"

> GET findALL to search all Professor in a List;
"professor/findAll"

> GET findOne to search one Professor in a List;
> "professor/findOne"

- Discipline;
> > POST to create a Discipline;
"disciplineDTO/"