package br.ufal.lccv.java.springboot.crud.crudprograd.controllers;

import br.ufal.lccv.java.springboot.crud.crudprograd.domain.builders.CourseBuilder;
import br.ufal.lccv.java.springboot.crud.crudprograd.domain.dtos.CourseDTO;
import br.ufal.lccv.java.springboot.crud.crudprograd.domain.inputs.CourseInput;
import br.ufal.lccv.java.springboot.crud.crudprograd.domain.models.Course;
import br.ufal.lccv.java.springboot.crud.crudprograd.services.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CourseController {

    private final CourseService service;

    @PostMapping("/")
    public CourseDTO create(@Valid @RequestBody CourseInput courseInput){
        Course course = service.create(courseInput);
        return CourseBuilder.build(course);
    }


}
